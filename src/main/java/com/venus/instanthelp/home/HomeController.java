package com.venus.instanthelp.home;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

	@RequestMapping("/")
	public String home() {
		return "<h1>Welcome Venu Madhav Reddy </h1>";
	}
}
